package plt;

public class Translator {
	
	private String inputPhrase;
	public final static String NIL = "nil"; 
	
	public Translator(String inputPhrase) {
		this.inputPhrase = inputPhrase;
	}

	public String getPhrase() {
		return inputPhrase;
	}
	
	public String translatePhrase() {
		String[] words = inputPhrase.split("\\s+");
		String finalPhrase = "";
		
		for (int i = 0; i < words.length; i++) {
			if( words[i].contains("-") ) {
				String[] wordsWithoutDash = words[i].split("-");
				for (int j = 0; j < wordsWithoutDash.length; j++) {
					wordsWithoutDash[ j ] = translateWord( wordsWithoutDash[ j ] );
				}
				words[i] = String.join("-", wordsWithoutDash);
			}
			else {
				if(  this.containsPunctuations( words[i] ) ) {
					String punctuation = getPunctuations( words[i] ) ;
					 words[i] = words[i].replaceAll("[^\\w]", "");
					 words[i] = translateWord( words[i] );
					 words[i] = words[i] + punctuation ;
					 
				}
				else {
					 words[i] = words[i].replaceAll("[^\\w]", "");
					 words[i] = translateWord( words[i] );
				}
				
			}
			finalPhrase = String.join(" ", words);
		   
		}
		
		
		return finalPhrase;
	}
	
	
	public String translateWord( String word ) {
		if( this.startsWithVowel( word )  ) {
			if( word.endsWith( "y" ) ) 	{
				return word.concat("nay");
			}
			else if( this.endsWithVowel() ) {
				return word.concat("yay");
			}
			else if( !this.endsWithVowel() ) {
				return word.concat("ay");
			}
		}
		if( !this.startsWithVowel( word ) && ( !word.equals("") )  ) {
			if( startsWithVowel( word.substring(1) ) ) {
				char firstCharacter = word.charAt( 0 );
				return ( word.substring(1) + firstCharacter + "ay" );
				
			}
			else {
				int i = 0;
				while( !( startsWithVowel( word.substring( i ) ) ) && ( i < word.length() )   ) {
					i++;
				}
				String initialConsonants = word.substring(0, i );
				return ( word.substring( i ) + initialConsonants + "ay" );
			}
		}
		
		return NIL;
	}

	public boolean  startsWithVowel( String inputPhrase ){
		if( inputPhrase.startsWith( "a" ) || inputPhrase.startsWith( "e" ) || inputPhrase.startsWith( "i" ) || inputPhrase.startsWith( "o" ) || inputPhrase.startsWith( "u" ) ) {
			return true;
		}
		return false;
	}
	
	public boolean endsWithVowel() {
		if( inputPhrase.endsWith( "a" ) || inputPhrase.endsWith( "e" ) || inputPhrase.endsWith( "i" ) || inputPhrase.endsWith( "o" ) || inputPhrase.endsWith( "u" ) ) {
			return true;
		}
		return false;
	}
	
	public boolean containsPunctuations( String inputPhrase ){
		if( inputPhrase.contains( "!" ) || inputPhrase.startsWith( "," ) || inputPhrase.startsWith( ";" ) || inputPhrase.startsWith( ":" ) || inputPhrase.startsWith( "?" ) || inputPhrase.startsWith( "'" ) || inputPhrase.startsWith( "()" ) ) {
			
			return true;
		}
		return false;
	}
	
	public String  getPunctuations( String inputPhrase ){
		if( inputPhrase.contains( "!" )  ) {
			return  "!";
		}
		else if(inputPhrase.startsWith( "," ) ) {
			return ",";
		}
		else if( inputPhrase.startsWith( ";" ) ) {
			return  ";";
		}
		else if(inputPhrase.startsWith( ":" ) ) {
			return  ":";
		}
		else if(inputPhrase.startsWith( "?" ) ) {
			return  "?";
		}
		else if(inputPhrase.startsWith( "'" ) ) {
			return  "'";
		}
		else if(inputPhrase.startsWith( "()" ) ) {
			return  "()";
		}
		return "";
	}
}
