package plt;

import static org.junit.Assert.*;

import org.junit.Test;

public class TranslatorTest {

	@Test
	public void testInputPhrase() {
		String inputPhrase = "hello world";
		
		Translator translator = new Translator( inputPhrase );
		
		assertEquals( "hello world", translator.getPhrase() );
	}
	
	@Test
	public void testTranslationEmptyPhrase() {
		String inputPhrase = "";
		
		Translator translator = new Translator( inputPhrase );
		
		assertEquals( Translator.NIL, translator.translatePhrase() );
	}

	@Test
	public void testTranslationPhraseStartingWithAEndigWithY() {
		String inputPhrase = "any";
		
		Translator translator = new Translator( inputPhrase );
		
		assertEquals( "anynay", translator.translatePhrase() );
	}
	
	@Test
	public void testTranslationPhraseStartingWithUEndigWithY() {
		String inputPhrase = "utility";
		
		Translator translator = new Translator( inputPhrase );
		
		assertEquals( "utilitynay", translator.translatePhrase() );
	}
	
	@Test
	public void testTranslationPhraseStartingWithVowelEndigWithVowel() {
		String inputPhrase = "apple";
		
		Translator translator = new Translator( inputPhrase );
		
		assertEquals( "appleyay", translator.translatePhrase() );
	}
	
	@Test
	public void testTranslationPhraseStartingWithVowelEndigWithConsonant() {
		String inputPhrase = "ask";
		
		Translator translator = new Translator( inputPhrase );
		
		assertEquals( "askay", translator.translatePhrase() );
	}
	
	@Test
	public void testTranslationPhraseStartingWithSingleConsonant() {
		String inputPhrase = "hello";
		
		Translator translator = new Translator( inputPhrase );
		
		assertEquals( "ellohay", translator.translatePhrase() );
	}
	
	@Test
	public void testTranslationPhraseStartingWithMoreConsonant() {
		String inputPhrase = "known";
		
		Translator translator = new Translator( inputPhrase );
		
		assertEquals( "ownknay", translator.translatePhrase() );
	}
	
	@Test
	public void testTranslationPhraseWithMoreWordsSeparatedWithSpace() {
		String inputPhrase = "hello world";
		
		Translator translator = new Translator( inputPhrase );
		
		assertEquals( "ellohay orldway", translator.translatePhrase() );
	}
	
	@Test
	public void testTranslationPhraseWithMoreWordsSeparatedWithDash() {
		String inputPhrase = "well-being";
		
		Translator translator = new Translator( inputPhrase );
		
		assertEquals( "ellway-eingbay", translator.translatePhrase() );
	}
	
	@Test
	public void testTranslationPhraseWithMoreWords() {
		String inputPhrase = "hello world well-being";
		
		Translator translator = new Translator( inputPhrase );
		
		assertEquals( "ellohay orldway ellway-eingbay", translator.translatePhrase() );
	}
	
	
	@Test
	public void testTranslationPhraseWithPunctuations() {
		String inputPhrase = "hello world!";
		
		Translator translator = new Translator( inputPhrase );
		
		assertEquals( "ellohay orldway!", translator.translatePhrase() );
	}
	
	
}
